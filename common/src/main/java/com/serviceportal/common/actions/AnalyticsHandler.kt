package com.serviceportal.common.actions

import android.app.Activity
import android.content.Context
import android.os.Bundle

interface AnalyticsHandler {
    fun logEvent(eventName: String, params: Bundle)
    fun initAnalytics(context: Context)
    fun addScreenTrack(activity: Activity, screenName: String)
    fun setUserId(userId: String)
    fun setDeviceAndUserId(userId: String, deviceId: String)
    fun removeUserId()
}
