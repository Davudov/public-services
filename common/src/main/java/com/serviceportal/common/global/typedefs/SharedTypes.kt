package com.serviceportal.common.global.typedefs
import androidx.annotation.StringDef

object SharedTypes {

    const val SETTINGS = "SETTINGS"
    const val REGISTRATION = "REGISTRATION"
    const val REVIEW = "REVIEW"
    const val WIDGET_TEMPLATE = "WIDGET_TEMPLATE"
    const val HIDE_BALANCE = "HIDE_BALANCE"
    const val HIDE_TRANSACTION = "HIDE_TRANSACTION"
    const val TRANSFERS = "TRANSFERS"

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(
        SETTINGS, REGISTRATION, REVIEW, WIDGET_TEMPLATE,HIDE_BALANCE, HIDE_TRANSACTION, TRANSFERS
    )
    annotation class SharedTypesDef
}