package com.serviceportal.core.navigation

import android.net.Uri
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.FragmentNavigator
import com.google.gson.Gson
import com.serviceportal.core.R

fun NavController.navigateBetweenModules(
    route: NavigationRoute,
    navOptions: NavOptions = NavOptions.Builder()
        .setEnterAnim(R.anim.slide_in_left)
        .setExitAnim(R.anim.slide_out_left)
        .setPopEnterAnim(R.anim.slide_in_right)
        .setPopExitAnim(R.anim.slide_out_right)
        .build(),
    extras: FragmentNavigator.Extras? = null
) {
    var deepLink = route.deeplink
    if (route.bundle != null) {
        val jsonData = Gson().toJson(route.bundle)
        deepLink = route.deeplink.replace("{data}", jsonData)
    } else {
        route.args.forEach { (k, v) ->
            deepLink = deepLink.replace("{$k}", v)
        }
    }
    navigate(Uri.parse(deepLink), navOptions, extras)
}