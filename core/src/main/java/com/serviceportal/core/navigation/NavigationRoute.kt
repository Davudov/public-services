package com.serviceportal.core.navigation

interface NavigationRoute {

    val deeplink: String
    val bundle: Any?
    val args: MutableMap<String, String>

}