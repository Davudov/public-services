package com.serviceportal.core.di.endpoint

import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.serviceportal.core.di.init.AppInitializers

@InstallIn(SingletonComponent::class)
@EntryPoint
interface AppInitializerEndpoint {

    fun getInitializers(): AppInitializers

}