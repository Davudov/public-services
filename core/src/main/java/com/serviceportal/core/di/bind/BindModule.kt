package com.serviceportal.core.di.bind

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import com.serviceportal.core.di.error.DefaultGlobalErrorLogger
import com.serviceportal.core.di.error.GlobalErrorHandler
import com.serviceportal.core.di.init.AppInitializer
import com.serviceportal.core.di.init.builtin.TimberInitializer
import com.serviceportal.core.di.state.DefaultStateFactory
import com.serviceportal.core.di.state.State
import com.serviceportal.core.di.state.StateFactory

@Module
@InstallIn(SingletonComponent::class)
abstract class BindModule {

    @IntoSet
    @Binds
    abstract fun bindTimberInitializer(initializer: TimberInitializer): AppInitializer

    @IntoSet
    @Binds
    abstract fun bindDefaultFactory(factory: DefaultStateFactory): StateFactory<State>

    @IntoSet
    @Binds
    abstract fun bindGlobalErrorHandler(errorLogger: DefaultGlobalErrorLogger): GlobalErrorHandler

}