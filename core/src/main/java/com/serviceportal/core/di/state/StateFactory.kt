package com.serviceportal.core.di.state

import androidx.databinding.Observable
import androidx.lifecycle.SavedStateHandle

interface StateFactory<T : State> {

    val stateClass: Class<*>

    fun getState(
        savedStateHandle: SavedStateHandle?
    ): T

    fun addObservers(
        state: T,
        callback: Observable.OnPropertyChangedCallback
    )

    fun removeObservers(
        state: T,
        callback: Observable.OnPropertyChangedCallback
    )

}