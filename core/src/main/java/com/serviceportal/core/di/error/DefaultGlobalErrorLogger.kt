package com.serviceportal.core.di.error

import com.serviceportal.core.mvvm.data.Fail
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DefaultGlobalErrorLogger @Inject constructor() : GlobalErrorHandler {

    override fun handleFailure(fail: Fail<*>) {
        Timber.e("Global error logger -> %s", fail.errorResponse.message)
    }

}