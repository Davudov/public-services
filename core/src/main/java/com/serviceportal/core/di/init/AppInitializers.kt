package com.serviceportal.core.di.init

import android.content.Context
import javax.inject.Inject

class AppInitializers @Inject constructor(
    private val initializers: Set<@JvmSuppressWildcards AppInitializer>
) {

    fun init(appContext: Context) {
        for (initializer in initializers) {
            initializer.initialize(appContext)
        }
    }

}