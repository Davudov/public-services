package com.serviceportal.core.di.state

import androidx.databinding.Observable
import androidx.lifecycle.SavedStateHandle
import javax.inject.Inject

class StateFactories @Inject constructor(
    private val stateFactories: Set<@JvmSuppressWildcards StateFactory<State>>
) {

    fun <T : Any> getState(
        kClass: Class<T>,
        savedStateHandle: SavedStateHandle?
    ): T {
        val factory = stateFactories.firstOrNull { it.stateClass == kClass }
        return factory?.getState(savedStateHandle) as T
    }


    fun <T : State> addObservers(
        state: T,
        kClass: Class<T>,
        callback: Observable.OnPropertyChangedCallback
    ) {
        val factory = stateFactories.firstOrNull { it.stateClass == kClass }
        factory?.addObservers(state, callback)
    }


    fun <T : State> removeObservers(
        state: T,
        kClass: Class<T>,
        callback: Observable.OnPropertyChangedCallback
    ) {
        val factory = stateFactories.firstOrNull { it.stateClass == kClass }
        factory?.removeObservers(state, callback)
    }

}