package com.serviceportal.core.di.state

import androidx.databinding.Observable
import androidx.lifecycle.SavedStateHandle
import javax.inject.Inject

class DefaultStateFactory @Inject constructor() : StateFactory<State> {

    override val stateClass: Class<State> = State::class.java

    override fun getState(
        savedStateHandle: SavedStateHandle?,
    ): State {
        return State()
    }

    override fun addObservers(state: State, callback: Observable.OnPropertyChangedCallback) {

    }

    override fun removeObservers(state: State, callback: Observable.OnPropertyChangedCallback) {

    }

}