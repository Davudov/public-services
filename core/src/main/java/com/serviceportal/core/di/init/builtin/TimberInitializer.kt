package com.serviceportal.core.di.init.builtin

import android.content.Context
import androidx.startup.Initializer
import com.serviceportal.core.BuildConfig
import com.serviceportal.core.di.init.AppInitializer
import timber.log.Timber
import javax.inject.Inject
class TimberInitializer @Inject constructor() : AppInitializer {

    override fun initialize(appContext: Context) {
        Timber.plant(Timber.DebugTree())
    }

}