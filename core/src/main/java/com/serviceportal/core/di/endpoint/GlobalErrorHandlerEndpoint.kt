package com.serviceportal.core.di.endpoint

import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.serviceportal.core.di.error.GlobalErrorHandlers

@InstallIn(SingletonComponent::class)
@EntryPoint
interface GlobalErrorHandlerEndpoint {

    fun getErrorHandlers(): GlobalErrorHandlers

}