package com.serviceportal.core.di.error

import com.serviceportal.core.mvvm.data.Fail
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GlobalErrorHandlers @Inject constructor(
    private val errorHandlers: Set<@JvmSuppressWildcards GlobalErrorHandler>
) {

    fun handleFailure(fail: Fail<*>) {
        errorHandlers.forEach {
            it.handleFailure(fail)
        }
    }

}