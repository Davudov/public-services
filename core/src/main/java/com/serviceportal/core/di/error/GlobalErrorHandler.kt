package com.serviceportal.core.di.error

import com.serviceportal.core.mvvm.data.Fail

interface GlobalErrorHandler {

    fun handleFailure(fail: Fail<*>)

}