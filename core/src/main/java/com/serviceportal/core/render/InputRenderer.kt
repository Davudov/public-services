package com.serviceportal.core.render

import androidx.databinding.Observable
import com.airbnb.epoxy.EpoxyModel

abstract class InputRenderer<O : Observable, M : EpoxyModel<*>> : Renderer<O, M>() {

    private val callback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            controller?.requestModelBuild()
        }
    }

    override fun renderItem(item: O): M {
        item.addOnPropertyChangedCallback(callback)
        return renderInputItem(item)
    }

    internal abstract fun renderInputItem(item: O): M

}