package com.serviceportal.core.render

import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.carousel

abstract class Renderer<T, M : EpoxyModel<*>> {

    private var list: List<T> = listOf()
    internal var controller: EpoxyController? = null
    private var beforeInterceptor: (EpoxyController.(T) -> Unit)? = null
    private var afterInterceptor: (EpoxyController.(T) -> Unit)? = null
    private var inCarousel: Boolean = false
    private var carouselId: String = ""

    fun with(controller: EpoxyController): Renderer<T, M> {
        this.controller = controller
        return this
    }

    fun item(item: T): Renderer<T, M> {
        list = listOf(item)
        return this
    }

    fun list(list: List<T>): Renderer<T, M> {
        this.list = list
        return this
    }

    fun beforeInterceptor(body: EpoxyController.(T) -> Unit): Renderer<T, M> {
        this.beforeInterceptor = body
        return this
    }

    fun afterInterceptor(body: EpoxyController.(T) -> Unit): Renderer<T, M> {
        this.afterInterceptor = body
        return this
    }

    fun inCarousel(carouselId: String): Renderer<T, M> {
        this.carouselId = carouselId
        this.inCarousel = true
        return this
    }

    fun render(body: M.(T) -> Unit = {}) {
        controller?.apply {
            if (inCarousel) {
                carousel {
                    id(carouselId)
                    models(list.map {
                        renderItem(it).apply {
                            body.invoke(this, it)
                        }
                    })
                }
            } else {
                list.forEach {
                    beforeInterceptor?.invoke(this, it)
                    renderItem(it).apply {
                        body.invoke(this, it)
                    }.addTo(this)
                    afterInterceptor?.invoke(this, it)
                }
            }
        }
    }

    internal abstract fun renderItem(item: T): M

}