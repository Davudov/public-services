package com.serviceportal.core.render

import androidx.databinding.Observable
import com.airbnb.epoxy.EpoxyModel
import javax.inject.Inject

class Renderers @Inject constructor() {

    fun <T, M : EpoxyModel<*>> custom(mapper: (T) -> M): Renderer<T, M> {
        return object : Renderer<T, M>() {
            override fun renderItem(item: T): M = mapper.invoke(item)
        }
    }

    fun <O : Observable, M : EpoxyModel<*>> customInput(mapper: (O) -> M): InputRenderer<O, M> {
        return object : InputRenderer<O, M>() {
            override fun renderInputItem(item: O): M = mapper.invoke(item)
        }
    }

}