package com.serviceportal.core.util

import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun EditText.debounceChanges(
    coroutineScope: CoroutineScope,
    timeMillis: Long,
    invoker: (String) -> Unit
) {
    var job: Job? = null
    doOnTextChanged { text, _, _, _ ->
        job?.cancel()
        job = coroutineScope.launch {
            delay(timeMillis)
            invoker.invoke(text?.toString() ?: "")
        }
    }
}