package com.serviceportal.core.util

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("visibleOrGone")
fun View.visibleOrGone(flag: Boolean) {
    this.visibility = if (flag) View.VISIBLE else View.GONE
}