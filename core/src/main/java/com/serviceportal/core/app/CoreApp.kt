package com.serviceportal.core.app

import android.app.Application
import dagger.hilt.EntryPoints
import com.serviceportal.core.di.endpoint.AppInitializerEndpoint
import com.serviceportal.core.di.endpoint.StateFactoryEndpoint
import com.serviceportal.core.di.state.StateFactories

open class CoreApp : Application() {

    val stateFactories: StateFactories by lazy {
        EntryPoints.get(this, StateFactoryEndpoint::class.java).getStateFactories()
    }

    override fun onCreate() {
        super.onCreate()
        // Initialize SDKs and eager setup modules
        EntryPoints.get(this, AppInitializerEndpoint::class.java)
            .getInitializers().init(this)
    }

}