package com.serviceportal.core.databinding


import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.serviceportal.core.util.observe
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class FragmentViewBindingDelegate<T : ViewDataBinding>(
    val fragment: Fragment,
    val beforeDestroyInvoker: () -> Unit
) : ReadOnlyProperty<Fragment, T> {

    private var binding: T? = null

    init {
        fragment.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onCreate(owner: LifecycleOwner) {
                fragment.observe(fragment.viewLifecycleOwnerLiveData) { viewLifecycleOwner ->
                    viewLifecycleOwner?.lifecycle?.addObserver(object : DefaultLifecycleObserver {
                        override fun onDestroy(owner: LifecycleOwner) {
                            beforeDestroyInvoker.invoke()
                            binding = null
                        }
                    })
                }
            }
        })
    }

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {

        val binding = binding

        if (binding != null && thisRef.view==binding.root) {
            return binding
        }

        val lifecycle = fragment.viewLifecycleOwner.lifecycle
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            throw IllegalStateException("Should not attempt to get bindings when Fragment views are destroyed.")
        }

        return DataBindingUtil.bind<T>(thisRef.requireView())!!.also { this.binding = it }
    }
}

fun <T : ViewDataBinding> Fragment.dataBinding(beforeDestroyInvoker: () -> Unit) =
    FragmentViewBindingDelegate<T>(this, beforeDestroyInvoker)