package com.serviceportal.core.mvvm.fragment.render

import android.os.Bundle
import android.view.View
import com.airbnb.epoxy.EpoxyController
import com.serviceportal.core.R
import com.serviceportal.core.databinding.FragmentCoreRenderBinding
import com.serviceportal.core.di.state.State
import com.serviceportal.core.mvvm.fragment.CoreMVVMFragment
import com.serviceportal.core.mvvm.viewmodel.CoreViewModel
import com.serviceportal.core.render.Renderers
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class CoreRenderFragment<S : State, VM : CoreViewModel<S>>(
    viewModelClass: KClass<VM>
) : CoreMVVMFragment<FragmentCoreRenderBinding, S, VM>(
    R.layout.fragment_core_render,
    viewModelClass
) {

    @Inject
    lateinit var renderers: Renderers

    open val content: EpoxyController.(S) -> Unit = {}
    open val header: EpoxyController.(S) -> Unit = {}
    open val footer: EpoxyController.(S) -> Unit = {}

    private val headerController: SimpleRenderController<S> by lazy {
        SimpleRenderController<S>().apply {
            callback = header
        }
    }
    private val footerController: SimpleRenderController<S> by lazy {
        SimpleRenderController<S>().apply {
            callback = footer
        }
    }
    private val contentController: SimpleRenderController<S> by lazy {
        SimpleRenderController<S>().apply {
            callback = content
            modelBuildRequested = {
                headerController.requestModelBuild()
                footerController.requestModelBuild()
            }
        }
    }

    /**
     * Set controllers and initial view setup
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.headerRecyclerView.setControllerAndBuildModels(headerController)
        binding.recyclerView.setControllerAndBuildModels(contentController)
        binding.bottomRecyclerView.setControllerAndBuildModels(footerController)
    }

    /**
     * Internal state change is used for model generation for 3 sections
     *
     * @param state
     */
    override fun internalStateChange(state: S) {
        super.internalStateChange(state)
        headerController.setStateAndBuild(state)
        contentController.setStateAndBuild(state)
        footerController.setStateAndBuild(state)
    }

    /**
     * State invalidation
     *
     * @param state
     */
    override fun invalidateState(state: S) {
        // Not needed in render fragment case
    }

    /**
     * Refresh all views and generate models
     *
     */
    fun refreshAllViews() {
        binding.headerRecyclerView.requestModelBuild()
        binding.recyclerView.requestModelBuild()
        binding.bottomRecyclerView.requestModelBuild()
    }

}