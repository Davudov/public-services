package com.serviceportal.core.mvvm.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Lifecycle
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.serviceportal.core.databinding.dataBinding
import com.serviceportal.core.mvvm.data.ParentBusyIndicatorHandler
import kotlinx.coroutines.CoroutineExceptionHandler
import timber.log.Timber

abstract class CoreFragment<VB : ViewDataBinding>(@LayoutRes val contentLayoutId: Int) :
    DialogFragment() {

    /**
     * View data binding
     */
    open val binding: VB by dataBinding(::onInternalPreDestroyView)

    /**
     * Set if this should behave as bottom sheet
     */
    open val isBottomSheet: Boolean = false

    open val bottomSheetState = BottomSheetBehavior.STATE_COLLAPSED

    /**
     * set if error dialog open on bottom
     */
    open val isDialogAsBottom=false

    /**
     * Determine if this fragment is registered as a dialog
     */
    open val behaveAsDialog: Boolean = false

    /**
     * Tracks if we are waiting for a dismissAllowingStateLoss or a regular dismiss once the
     * BottomSheet is hidden and onStateChanged() is called.
     */
    private var waitingForDismissAllowingStateLoss = false

    /**
     * Coroutine exception handler for ui states
     */
    val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Timber.e(throwable)
    }

    /**
     * onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showsDialog = behaveAsDialog
    }

    /**
     * onCreateDialog
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return if (isBottomSheet) CoreBottomSheetDialog(
            requireContext(),
            theme
        ) else super.onCreateDialog(
            savedInstanceState
        )
    }

    /**
     * Dismiss dialog or bottom sheet dialog
     */
    override fun dismiss() {
        if (isBottomSheet) {
            if (!tryDismissWithAnimation(false)) {
                super.dismiss()
            }
        } else {
            super.dismiss()
        }
    }

    override fun dismissAllowingStateLoss() {
        if (isBottomSheet) {
            if (!tryDismissWithAnimation(true)) {
                super.dismissAllowingStateLoss()
            }
        } else {
            super.dismissAllowingStateLoss()
        }
    }

    private fun dismissWithAnimation(
        behavior: BottomSheetBehavior<*>, allowingStateLoss: Boolean
    ) {
        waitingForDismissAllowingStateLoss = allowingStateLoss
        if (behavior.state == BottomSheetBehavior.STATE_HIDDEN) {
            dismissAfterAnimation()
        } else {
            dialog?.let {
                if (it is CoreBottomSheetDialog) it.removeDefaultCallback()
            }
            behavior.addBottomSheetCallback(BottomSheetDismissCallback(this))
            behavior.setState(BottomSheetBehavior.STATE_HIDDEN)
        }
    }

    private fun dismissAfterAnimation() {
        if (waitingForDismissAllowingStateLoss) {
            super.dismissAllowingStateLoss()
        } else {
            super.dismiss()
        }
    }

    /**
     * Tries to dismiss the dialog fragment with the bottom sheet animation. Returns true if possible,
     * false otherwise.
     */
    private fun tryDismissWithAnimation(allowingStateLoss: Boolean): Boolean {
        val baseDialog = dialog
        if (baseDialog is BottomSheetDialog) {
            val dialog = baseDialog
            val behavior: BottomSheetBehavior<*> = dialog.behavior
            if (behavior.isHideable && dialog.dismissWithAnimation) {
                dismissWithAnimation(behavior, allowingStateLoss)
                return true
            }
        }
        return false
    }

    /**
     * onCreateView
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(contentLayoutId, container, false)
    }

    override fun onStart() {
        super.onStart()
        // Dialog window setup
        dialog?.window?.apply {
            setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            val backColor = ContextCompat.getColor(requireContext(), android.R.color.transparent)
            setBackgroundDrawable(ColorDrawable(backColor))

            // error dialogs show bottom
            if (isDialogAsBottom){
                setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                val wlp: WindowManager.LayoutParams = attributes
                wlp.gravity = Gravity.BOTTOM
                wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
                attributes = wlp
            }

        }
        if (isBottomSheet) {
            (dialog as? CoreBottomSheetDialog)?.behavior?.state = bottomSheetState
        }
    }

    /**
     *
     */
    internal open fun onInternalPreDestroyView() {
        // Expose method for inheritance
        onPreDestroyView()
    }

    /**
     * This method is called before destroying data binding
     */
    open fun onPreDestroyView() {
        // NoImpl
    }

    /**
     * Show busy indicator from parent activity if supported
     *
     * @param show
     */
    open fun showBusyIndicator(show: Boolean) {
        try {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (requireActivity() as? ParentBusyIndicatorHandler)?.showBusyIndicator(show)
            }
        } catch (ex: Exception) {
            Timber.e(ex)
        }
    }

    /**
     * Extension method for String Res
     */
    fun Int.str(param: String? = null): String {
        try {
            return if (param == null) requireContext().getString(this) else requireContext().getString(
                this,
                param
            )
        } catch (ex: Exception) {
            Timber.w("Get string threw exception")
        }
        return ""
    }

    /**
     * Extension method for String Res
     */
    fun Int.str(param: Int): String {
        try {
            return requireContext().getString(this, param)
        } catch (ex: Exception) {
            Timber.w("Get string threw exception")
        }
        return ""
    }

    /***
     *  Share any text easily with intents
     */
    fun shareItem(text: String) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, text)
        sendIntent.type = "text/plain"
        val pm = activity?.packageManager
        if (pm != null && sendIntent.resolveActivity(pm) != null) {
            startActivity(sendIntent)
        } else {
            Timber.e("No Intent available to handle share action")
        }
    }

    /**
     * Utility method for hiding keyboard
     */
    fun hideSoftKeyboard() {
        try {
            val inputMethodManager =
                context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(view?.windowToken, 0)
        } catch (e: Exception) {
            Timber.e(e, "Hide keyboard")
        }
    }


    /**
     * Get typed data from bundle arguments, which is sent by navigation data key
     *
     * @param B
     * @param klass
     * @return
     */
    fun <B> argumentAs(klass: Class<B>): B? {
        val jsonData = arguments?.getString("data")
        if (jsonData != null && jsonData.isNotEmpty()) {
            return Gson().fromJson(jsonData, klass)
        }
        return null
    }


    class BottomSheetDismissCallback(private val coreFragment: CoreFragment<*>) :
        BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                coreFragment.dismissAfterAnimation()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

}