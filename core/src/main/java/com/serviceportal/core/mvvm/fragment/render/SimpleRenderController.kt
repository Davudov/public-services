package com.serviceportal.core.mvvm.fragment.render

import com.airbnb.epoxy.EpoxyController

class SimpleRenderController<S> : EpoxyController() {

    var callback: EpoxyController.(S) -> Unit = {}
    var modelBuildRequested: () -> Unit = {}

    private var state: S? = null

    fun setStateAndBuild(state: S) {
        this.state = state
        requestModelBuild()
    }

    override fun buildModels() {
        state?.let {
            callback.invoke(this, it)
        }
    }

    override fun requestModelBuild() {
        modelBuildRequested.invoke()
        super.requestModelBuild()
    }

}