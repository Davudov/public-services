package com.serviceportal.core.mvvm.data

import androidx.annotation.StringRes

sealed class Event {

    class Alert(
        val title: String,
        val message: String,
        @StringRes val titleRes: Int? = null,
        @StringRes val messageRes: Int? = null
    ) : Event()

    class SnackBar(val title: String) : Event()
    class Toast(val title: String) : Event()
    class Busy(val show: Boolean) : Event()

}