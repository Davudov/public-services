package com.serviceportal.core.mvvm.fragment

import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.*
import com.serviceportal.core.di.state.State
import com.serviceportal.core.mvvm.viewmodel.CoreViewModel
import kotlin.reflect.KClass

abstract class CoreRecyclerViewFragment<VB : ViewDataBinding, S : State, VM : CoreViewModel<S>, C : EpoxyController>(
    @LayoutRes contentLayoutId: Int,
    viewModelClass: KClass<VM>
) : CoreMVVMFragment<VB, S, VM>(contentLayoutId, viewModelClass) {


    /**
     * Visibility tracker used by epoxy recycler view
     */
    private var visibilityTracker: EpoxyVisibilityTracker? = null

    /**
     * Epoxy controller to bind to the recycler view
     */
    abstract var controller: C

    /**
     * Enable/Disable epoxy model visibility tracking
     */
    open val trackModelVisibility: Boolean = false

    /**
     * Enable/Disable item decoration
     */
    open val useItemDecoration: Boolean = false

    /**
     * Recycler view to bind
     */
    abstract fun recyclerView(): EpoxyRecyclerView

    /**
     * Invalidate item decorations
     */
    override fun internalStateChange(state: S) {
        if (useItemDecoration) recyclerView().invalidateItemDecorations()
    }

    /**
     * OnViewCreated of the fragment
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set controller
        recyclerView().setController(controller)
        // Attach model visibility tracker
        if (trackModelVisibility) {
            visibilityTracker = EpoxyVisibilityTracker()
            visibilityTracker?.attach(recyclerView())
        }
        // Add item decoration
        if (useItemDecoration) recyclerView().addItemDecoration(itemDecoration)
    }

    /**
     * Clean up recycler view instances and dependencies
     */
    override fun onInternalPreDestroyView() {
        super.onInternalPreDestroyView()
        // Detach visibility tracker
        if (trackModelVisibility) visibilityTracker?.detach(recyclerView())
        // Remove item decoration
        if (useItemDecoration) recyclerView().removeItemDecoration(itemDecoration)
    }

    override fun onDestroy() {
        // Cancel model build on fragment destroy
        controller.cancelPendingModelBuild()
        super.onDestroy()
    }

    open fun setItemOffset(outRect: Rect, model: EpoxyModel<*>, position: Int, view: View) {

    }

    open fun setDrawOver(
        c: Canvas,
        parent: RecyclerView,
        state: RecyclerView.State,
        model: EpoxyModel<*>
    ) {

    }

    private val itemDecoration = object : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            try {
                val position = parent.getChildAdapterPosition(view)
                if (position == -1) return
                val adapter = parent.adapter
                if (adapter is EpoxyControllerAdapter) {
                    val model = adapter.getModelAtPosition(position)
                    setItemOffset(outRect, model, position, view)
                }
            } catch (ex: Exception) {

            }
        }

        override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            try {
                val childCount = parent.adapter?.itemCount ?: 0
                val adapter = parent.adapter
                if (adapter is EpoxyControllerAdapter) {
                    val model = adapter.getModelAtPosition(childCount - 1)
                    setDrawOver(c, parent, state, model)
                }
            } catch (ex: Exception) {

            }
        }

    }

}
