package com.serviceportal.core.mvvm.data

import kotlinx.coroutines.flow.Flow

interface DataRequest<in Params, Data> {

    suspend fun runAsFlow(params: Params): Flow<Async<Data>>

    val showBusyIndicatorWhileProcessing: Boolean

}