package com.serviceportal.core.mvvm.viewmodel

import androidx.databinding.Observable
import androidx.lifecycle.*
import com.serviceportal.core.di.state.State
import com.serviceportal.core.di.state.StateFactories
import com.serviceportal.core.mvvm.data.*
import com.serviceportal.core.mvvm.events.SingleLiveEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

@OptIn(InternalCoroutinesApi::class)
abstract class CoreViewModel<S : State>(
    val kClass: KClass<S>,
    val savedStateHandle: SavedStateHandle?
) : ViewModel() {

    var useTestCoroutineScope: Boolean = false

    /**
     * Holds the route for navigation component
     */
    internal val failureEvents: MutableSharedFlow<Fail<*>> = MutableSharedFlow(0, 24)

    /**
     * Single triggering events
     */
    val event: SingleLiveEvent<Event> =
        SingleLiveEvent()

    /**
     * Top level exception handler for the included context
     */
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable -> Timber.e(throwable) }

    /**
     * View state representing the UI status of bound fragment or activity
     */
    private lateinit var initialState: S
    val state: MutableStateFlow<S> by lazy {
        MutableStateFlow(initialState)
    }

    /**
     * Mutex object for synchronized state management with locked/unlocked toggles
     */
    private val stateMutex = Mutex()

    /**
     * LiveData representation of the view state
     */
    val liveData: LiveData<S> get() = state.asLiveData()

    /**
     * Internal invalidate view event
     */
    internal val invalidateView = SingleLiveEvent<Boolean>()

    /**
     * Observable refresh callback to be called when observable in state object changes.
     */
    private val refreshCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            invalidateView.postValue(true)
        }
    }

    /**
     * Initialize view model and the state
     */
    init {
        // Lock state until arguments are resolved and initial state created
        launch(exceptionHandler) {
            stateMutex.lock()
        }
    }

    /**
     * Set state factories
     *
     * @param stateFactories
     */
    fun setStateFactories(stateFactories: StateFactories) {
        if (this::initialState.isInitialized) return
        initialState = stateFactories.getState(kClass.java, savedStateHandle)
        if (stateMutex.isLocked) stateMutex.unlock()
    }

    /**
     * Returns a snapshot of the current state.
     */
    fun currentState(): S = state.value


    /**
     * Attach listeners to the databinding observables in the state
     *
     * @param stateFactories
     */
    fun attachDataBindingObservers(stateFactories: StateFactories){
        withState {
            stateFactories.addObservers(it, kClass.java, refreshCallback)
        }
    }

    /**
     * Detach listeners to the databinding observables in the state
     *
     * @param stateFactories
     */
    fun detachDataBindingObservers(stateFactories: StateFactories){
        withState {
            stateFactories.removeObservers(it, kClass.java, refreshCallback)
        }
    }


    /**
     * Subscribe to changes happening in the state
     */
    protected fun subscribe(block: (S) -> Unit) {
        launch(exceptionHandler) {
            val awaitedState = stateMutex.withLock {
                state
            }
            awaitedState.collect { block(it) }
        }
    }

    /**
     * Subscribe to changes happening in the state fields
     */
    protected fun <A> selectSubscribe(prop1: KProperty1<S, A>, block: (A) -> Unit) {
        launch(exceptionHandler) {
            val awaitedState = stateMutex.withLock {
                state
            }
            awaitedState.map { prop1.get(it) }.distinctUntilChanged().collect { block(it) }
        }
    }

    /**
     * Set state with current state's reducer
     */
    protected fun setState(reducer: S.() -> S) {
        setStateLocking(reducer)
    }

    /**
     * Set state with locking in the mutex
     */
    @OptIn(ExperimentalCoroutinesApi::class)
    private fun setStateLocking(reducer: S.() -> S) {
        launch(exceptionHandler) {
            stateMutex.withLock {
                state.value = reducer(state.value)
            }
        }
    }

    /**
     * Get state with locking in the mutex
     */
    @OptIn(ExperimentalCoroutinesApi::class)
    private fun withStateLocking(block: (S) -> Unit) {
        launch(exceptionHandler) {
            stateMutex.withLock {
                block(state.value)
            }
        }
    }

    fun withState(block: (S) -> Unit) {
        withStateLocking(block)
    }


    @OptIn(InternalCoroutinesApi::class)
    protected fun <T> Flow<T>.collectSetState(reducer: S.(T) -> S) {
        launch {
            this@collectSetState.flowOn(Dispatchers.IO)
                .collect { item -> setState { reducer(item) } }
        }
    }

    protected fun <T> (suspend () -> T?).asState(reducer: S.(T?) -> S) {
        launch {
            val result = this@asState.invoke()
            this@CoreViewModel.setState { reducer(result) }
        }
    }

    protected fun <T> Flow<Async<T>>.collectExecute(reducer: S.(Async<T>) -> S) {
        launch {
            this@collectExecute.collect {
                proceedAsyncFail(it)
                // Set to state
                this@CoreViewModel.setState { reducer(it) }
            }
        }
    }

    protected fun <T, R> DataRequest<T, R>.execute(params: T, reducer: S.(Async<R>) -> S) {
        launch {
            this@execute.runAsFlow(params).collect {
                // Loading indicator state
                if (showBusyIndicatorWhileProcessing) {
                    event.postValue(Event.Busy(it is Loading))
                }
                proceedAsyncFail(it)
                // Set to state
                this@CoreViewModel.setState { reducer(it) }
            }
        }
    }

    protected fun <T> proceedAsyncFail(async: Async<T>) {
        if (async is Fail<*>) {
            proceedFail(async)
        }
    }

    protected fun proceedFail(fail: Fail<*>) {
        // Show alert states if needed
        when (fail) {
            is Fail.Alert -> Event.Alert(
                fail.title,
                fail.message,
                fail.titleRes,
                fail.messageRes
            )
            is Fail.SnackBar -> Event.SnackBar(fail.title)
            is Fail.Toast -> Event.Toast(fail.title)
            else -> null
        }?.also { event.postValue(it) }
        // Send any failure to another pipeline
        failureEvents.tryEmit(fail)
    }

    fun launch(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> Unit
    ) {
        if (useTestCoroutineScope) {
            runBlocking(context, block)
        } else {
            viewModelScope.launch(context + exceptionHandler, start, block)
        }
    }

}
