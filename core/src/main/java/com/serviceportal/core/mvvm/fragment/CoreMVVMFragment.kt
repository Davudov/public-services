package com.serviceportal.core.mvvm.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.serviceportal.core.app.CoreApp
import com.serviceportal.core.di.error.GlobalErrorHandlers
import com.serviceportal.core.di.state.State
import com.serviceportal.core.di.state.StateFactories
import com.serviceportal.core.mvvm.data.Event
import com.serviceportal.core.mvvm.data.Fail
import com.serviceportal.core.mvvm.viewmodel.CoreViewModel
import com.serviceportal.core.util.observe
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

abstract class CoreMVVMFragment<VB : ViewDataBinding, S : State, VM : CoreViewModel<S>>(
    @LayoutRes contentLayoutId: Int,
    viewModelClass: KClass<VM>
) : CoreFragment<VB>(contentLayoutId) {

    @Inject
    lateinit var globalErrorHandlers: GlobalErrorHandlers

    private var stateInitialized: Boolean = false

    private val stateFactories: StateFactories? by lazy {
        (requireActivity().application as? CoreApp)?.stateFactories
    }

    open val viewModelFactoryOwner: (() -> ViewModelStoreOwner) = { this }

    open val factoryProducer: ViewModelProvider.Factory by lazy { defaultViewModelProviderFactory }

    open val viewModel: VM by createViewModelLazy(
        viewModelClass,
        { viewModelFactoryOwner.invoke().viewModelStore },
        { factoryProducer })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeStateAndViewModel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeStateAndViewModel()
    }

    override fun onDestroy() {
        stateInitialized = false
        super.onDestroy()
    }

    private fun initializeStateAndViewModel() {
        try {
            if (stateInitialized) return
            // Set state factory
            stateFactories?.let(viewModel::setStateFactories)
            stateFactories?.let(viewModel::attachDataBindingObservers)
            // Listen to view state changes
            observe(viewModel.liveData) {
                if (it != null) {
                    invalidateState(it)
                    internalStateChange(it)
                }
            }
            // Listen to invalidate view
            observe(viewModel.invalidateView) {
                val state = viewModel.currentState()
                invalidateState(state)
            }
            // Listen to navigation route
            addRepeatingJob(Lifecycle.State.STARTED) {
                viewModel.failureEvents.collectLatest {
                    handleAsyncFailures(it)
                }
            }
            // Listen to events
            observe(viewModel.event) { event ->
                event?.let { handleEvent(it) }
            }
            stateInitialized = true
        } catch (ex: Exception) {
            throw IllegalStateException(ex)
        }
    }

    private fun handleAsyncFailures(fail: Fail<*>) {
        globalErrorHandlers.handleFailure(fail)
    }

    open fun handleEvent(event: Event) {
        when (event) {
            is Event.Alert -> showAlert(event)
            is Event.SnackBar -> showSnackBar(event)
            is Event.Toast -> showToast(event)
            is Event.Busy -> showBusyIndicator(event.show)
        }
    }

    open fun showToast(event: Event.Toast) {
        Toast.makeText(requireContext(), event.title, Toast.LENGTH_SHORT).show()
    }

    open fun showSnackBar(event: Event.SnackBar) {
        try {
            view?.let {
                Snackbar.make(it, event.title, Snackbar.LENGTH_SHORT)
            }
        } catch (ex: Exception) {
            Timber.e(ex)
        }
    }

    open fun showAlert(event: Event.Alert) {
        val title = event.titleRes?.let { getString(it) } ?: event.title
        val message = event.messageRes?.let { getString(it) } ?: event.message
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Refresh state
        invalidateState(viewModel.currentState())
    }

    /**
     * Invalidate state is called whenever state in the [VM] changes
     *
     * @param state
     */
    abstract fun invalidateState(state: S)

    /**
     * Internal use only, state changes reflected for base classes
     *
     * @param state
     */
    internal open fun internalStateChange(state: S) {
        // NoImpl
    }

    /**
     * Subscribe property of state and gather the value in lifecycle aware manner
     *
     * @param A
     * @param prop1
     * @param block
     */
    fun <A : Any, S : State> CoreViewModel<S>.selectSubscribe(
        prop1: KProperty1<S, A>,
        block: (A) -> Unit
    ) {
        this@CoreMVVMFragment.observe(liveData.map(prop1).distinctUntilChanged()) {
            if (it != null) block.invoke(it)
        }
    }

}
