package com.serviceportal.core.mvvm.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ErrorResponse(
    @SerializedName("status")
    val status: Int? = null,

    @SerializedName("code")
    val code: String? = null,

    @SerializedName("message")
    val message: String? = null,

    @SerializedName("detail")
    val detail: String? = null,

    @SerializedName("type")
    val type: String? = null,

    @SerializedName("timestamp")
    val timestamp: String? = null,

    @SerializedName("path")
    val path: String? = null,

    @SerializedName("help_url")
    val help_url: String? = null,

    @SerializedName("requested_lang")
    val requested_lang: String? = null,

    @SerializedName("provided_lang")
    val provided_lang: String? = null,

    @SerializedName("data")
    var data: ErrorDataModel? = null,

    @SerializedName("description_android")
    var descriptionAndroid: String? = null,

    @SerializedName("error_code")
    var errorCode: String? = null
)

@Parcelize
data class ErrorDataModel (
    @SerializedName("phones")
    var phoneList: ArrayList<ErrorPhoneModel>? = null
) : Parcelable

@Parcelize
data class ErrorPhoneModel (
    @SerializedName("number")
    var number: String? = null
) : Parcelable
