package com.serviceportal.core.mvvm.data

interface ParentBusyIndicatorHandler {

    fun showBusyIndicator(show: Boolean)

}