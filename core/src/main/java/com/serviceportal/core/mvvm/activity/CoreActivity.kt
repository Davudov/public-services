package com.serviceportal.core.mvvm.activity

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.serviceportal.core.app.CoreApp
import com.serviceportal.core.di.state.State
import com.serviceportal.core.mvvm.data.ParentBusyIndicatorHandler
import com.serviceportal.core.mvvm.viewmodel.CoreViewModel
import com.serviceportal.core.util.observe
import kotlin.reflect.KClass

abstract class CoreActivity<S : State, VM : CoreViewModel<S>>(viewModelClass: KClass<VM>) :
    AppCompatActivity(), ParentBusyIndicatorHandler {

    open val viewModelFactoryOwner: (() -> ViewModelStoreOwner) = { this }

    open val factoryProducer: ViewModelProvider.Factory by lazy { defaultViewModelProviderFactory }

    open val viewModel: VM by ViewModelLazy(viewModelClass,
        { viewModelFactoryOwner.invoke().viewModelStore },
        { factoryProducer })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set state factory
        (application as? CoreApp)?.stateFactories?.let(viewModel::setStateFactories)
        // Listen to view state changes
        observe(viewModel.liveData) {
            if (it != null) {
                invalidateState(it)
            }
        }
    }

    /**
     * Invalidate state is called whenever state in the [VM] changes
     */
    abstract fun invalidateState(state: S)

    /**
     * Helper method to get controller of a container
     *
     * @param containerId
     * @return
     */
    fun getControllerOf(@IdRes containerId: Int): NavController {
        return (supportFragmentManager.findFragmentById(containerId) as NavHostFragment)
            .navController
    }

    open fun rootController(): NavController? = null

}