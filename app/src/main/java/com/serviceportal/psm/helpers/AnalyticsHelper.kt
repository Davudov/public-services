
package com.serviceportal.psm.helpers

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics

/**
 * This object class is Analytics helper class. Hanalyticas helper class enables to connect to analytics server and send
 * data to there. Currently we are sending screen names and parameters as a measure data on analytics side
 */
object AnalyticsHelper {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    /**
     * Init analytics  by context
     */
    fun initAnalytics(context: Context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context)
    }

    /**
     * Log the main events to measure. Such as payment success or pay result, Login actions, transfer actions and o.
     * @param eventName Which event category name do you want to send ( Ex. Payment)
     * @param params Which parameters do you want to send ( Ex. is_success)
     * @see Events
     * @see Params
     */
    fun logEvent(eventName: String, params: Bundle) {
        mFirebaseAnalytics?.logEvent(eventName, params)
    }

    /**
     * Log screen name. This screen name will be main container fragment
     * @param activity from which activity screen name will start
     * @param screenName which screen name you will set. The main screen name look [ScreenNames]
     * @see ScreenNames
     */
    fun addScreenTrack(activity: Activity, screenName: String) {
        mFirebaseAnalytics?.setCurrentScreen(activity, screenName, null)
    }

    /**
     * Sets user id to firebase to track it at the future
     * @param userId user credential which can be came from kobil or back end service
     */
    fun setUserId(userId: String) {
        try {
            mFirebaseAnalytics?.setUserId(userId)
            FirebaseCrashlytics.getInstance().setUserId(userId)
        } catch (e: Exception) {
            //log it at the future
        }
    }

    /**
     * removes  user id to firebase
     */
    fun removeUserId() {
        try {
            mFirebaseAnalytics?.setUserId(null)
            FirebaseCrashlytics.getInstance().setUserId("")
        } catch (e: Exception) {
            //log it at the future
        }
    }

    /**
     * This object class is responsible to analyse firebase analytics screens
     * If there is new screens you should add this screen name into here for future analysis
     */
    object ScreenNames {

    }

    /**
     * This object class is responsible to analyse firebase analytics events
     * if there are any action type to log into firebase it can be added into here and use at future
     * to log events
     */
    object Events {

    }

    /**
     * This object class is responsible to analyse firebase analytics even parameter
     */
    object Params {

    }

}