package com.serviceportal.psm.helpers

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.serviceportal.common.actions.AnalyticsHandler
import com.serviceportal.psm.helpers.AnalyticsHelper.Events
import com.serviceportal.psm.helpers.AnalyticsHelper.Params
import com.serviceportal.psm.helpers.AnalyticsHelper.ScreenNames
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AnalyticsHandlerImpl @Inject constructor() : AnalyticsHandler {

    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    /**
     * Init analytics  by context
     */
    override fun initAnalytics(context: Context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context)
    }

    /**
     * Log the main events to measure. Such as payment success or pay result, Login actions, transfer actions and o.
     * @param eventName Which event category name do you want to send ( Ex. Payment)
     * @param params Which parameters do you want to send ( Ex. is_success)
     * @see Events
     * @see Params
     */
    override fun logEvent(eventName: String, params: Bundle) {
        mFirebaseAnalytics?.logEvent(eventName, params)
    }

    /**
     * Log screen name. This screen name will be main container fragment
     * @param activity from which activity screen name will start
     * @param screenName which screen name you will set. The main screen name look [ScreenNames]
     * @see ScreenNames
     */
    override fun addScreenTrack(activity: Activity, screenName: String) {
        mFirebaseAnalytics?.setCurrentScreen(activity, screenName, null)
    }

    /**
     * Sets user id to firebase to track it at the future
     * @param userId user credential which can be came from kobil or back end service
     */
    override fun setUserId(userId: String) {
        try {
            mFirebaseAnalytics?.setUserId(userId)
            FirebaseCrashlytics.getInstance().setUserId(userId)
        } catch (e: Exception) {
            //log it at the future
        }
    }

    /**
     * Sets device id and external user id to firebase to track
     */
    override fun setDeviceAndUserId(userId: String, deviceId: String) {
        try {
            FirebaseCrashlytics.getInstance().setCustomKey("device-id", deviceId)
            FirebaseCrashlytics.getInstance().setCustomKey("external-user-id", userId)
        } catch (e: Exception) {
            //log it at the future
        }
    }

    /**
     * removes  user id to firebase
     */
    override fun removeUserId() {
        try {
            mFirebaseAnalytics?.setUserId(null)
            FirebaseCrashlytics.getInstance().setUserId("")
        } catch (e: Exception) {
            //log it at the future
        }
    }


}