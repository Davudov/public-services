package com.serviceportal.psm.state

import com.serviceportal.annotations.ViewState
import com.serviceportal.core.di.state.State
import timber.log.Timber

@ViewState
data class MainState(
    var animationEnded: Boolean = false,
) : State() {
    fun toggleAnimation() {
        animationEnded = animationEnded.not()
        Timber.d("toggleAnimation $animationEnded")
    }
}

