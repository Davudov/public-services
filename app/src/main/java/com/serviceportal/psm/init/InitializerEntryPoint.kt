package com.serviceportal.psm.init

import android.content.Context
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent

@EntryPoint
@InstallIn(SingletonComponent::class)
interface InitializerEntryPoint {

    companion object {

        fun resolve(context: Context): InitializerEntryPoint {
            val appContext = context.applicationContext ?: throw IllegalStateException()
            return EntryPointAccessors.fromApplication(
                appContext,
                InitializerEntryPoint::class.java
            )
        }

    }

    fun inject(mainInitializer: MainInitializer)
    fun inject(analyticInitializer: FirebaseAnalyticInitializer)
    fun inject(remoteConfigInitializer: RemoteConfigInitializer)
}