package com.serviceportal.psm.init

import android.content.Context
import androidx.startup.Initializer
import com.google.firebase.FirebaseApp
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.serviceportal.common.global.typedefs.SharedTypes
import com.serviceportal.common.helper.MainSharedPreferences
import com.serviceportal.common.utils.SharedConstants
import com.serviceportal.data.base.network.interceptors.DeviceManager
import com.serviceportal.psm.manage.IOCoroutineScope
import com.serviceportal.psm.util.StaticDatas
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import javax.inject.Inject

class RemoteConfigInitializer : Initializer<Unit>, IOCoroutineScope {

    companion object {
        const val minimumFetchIntervalInSeconds = 3600L
        const val fetchTimeoutInSeconds = 180L
    }

    @Inject
    lateinit var deviceManager: DeviceManager

    override fun create(context: Context) {
        launch {
            // Inject
            InitializerEntryPoint.resolve(context).inject(this@RemoteConfigInitializer)
            // Set FCM Token
            val fcmToken: String? = MainSharedPreferences(context,
                SharedTypes.SETTINGS).get(SharedConstants.FIREBASETOKEN, null)
            StaticDatas.fcmToken = fcmToken
            deviceManager.setFcmToken(fcmToken)
            // Init firebase
            FirebaseApp.initializeApp(context)
            val remoteConfig = FirebaseRemoteConfig.getInstance()
            // Fetch and activate
            val settings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(minimumFetchIntervalInSeconds)
                .setFetchTimeoutInSeconds(fetchTimeoutInSeconds).build()
            remoteConfig.setConfigSettingsAsync(settings)
            remoteConfig.setDefaultsAsync(com.serviceportal.common.R.xml.remote_config_defaults)
            remoteConfig.fetchAndActivate().addOnFailureListener { }.await()
            Timber.d("Initialization of RemoteConfig completed")
        }
        return
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> {
        return mutableListOf(TimberInitializer::class.java, InjectInitializer::class.java)
    }

}