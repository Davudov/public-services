package com.serviceportal.psm.init

import android.content.Context
import androidx.startup.Initializer
import com.serviceportal.common.actions.AnalyticsHandler
import com.serviceportal.psm.manage.IOCoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class FirebaseAnalyticInitializer : Initializer<Unit>, IOCoroutineScope {

    @Inject
    lateinit var analyticsHandler: AnalyticsHandler

    override fun create(context: Context) {
        launch {
            //Inject
            InitializerEntryPoint.resolve(context).inject(this@FirebaseAnalyticInitializer)
            //create instance
            analyticsHandler.initAnalytics(context)
            Timber.d("Initialization of FirebaseAnalytics completed")
        }
        return
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> {
        return mutableListOf(
            InjectInitializer::class.java,
            TimberInitializer::class.java,
        )
    }
}