package com.serviceportal.psm.init

import android.content.Context
import androidx.startup.Initializer
import timber.log.Timber

class InjectInitializer : Initializer<Unit> {

    override fun create(context: Context) {
        // Lazily initialize ApplicationComponent before Application's `onCreate`
        InitializerEntryPoint.resolve(context)
        Timber.d("Initialization of InjectInitializer completed")
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> {
        return mutableListOf()
    }

}