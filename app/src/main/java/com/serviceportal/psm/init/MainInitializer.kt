package com.serviceportal.psm.init

import android.content.Context
import androidx.startup.Initializer

class MainInitializer : Initializer<Unit> {


    override fun create(context: Context) {
        InitializerEntryPoint.resolve(context).inject(this)
        return
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> {
        return mutableListOf(
            TimberInitializer::class.java,
            RemoteConfigInitializer::class.java,
            InjectInitializer::class.java,
            FirebaseAnalyticInitializer::class.java,
        )
    }

}