package com.serviceportal.psm.di

import com.serviceportal.common.actions.AnalyticsHandler
import com.serviceportal.data.base.config.AppConfig
import com.serviceportal.psm.helpers.AnalyticsHandlerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppBindModule {

    @Binds
    @Singleton
    abstract fun bindAnalyticsHandler(analyticsHandlerImpl: AnalyticsHandlerImpl): AnalyticsHandler

    @Binds
    @Singleton
    abstract fun bindAppConfig(configImpl: AppConfigImpl): AppConfig
}