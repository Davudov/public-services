package com.serviceportal.psm.di

import android.content.Context
import android.os.Build
import com.serviceportal.data.base.config.AppConfig
import com.serviceportal.data.models.UserInformation
import com.serviceportal.psm.BuildConfig
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppConfigImpl @Inject constructor(
    @ApplicationContext val context: Context,
) : AppConfig {

    override val isProduction: Boolean
        get() = BuildConfig.FLAVOR == "prod"

    override val osVersion: String
        get() = Build.VERSION.RELEASE

    override val appBuildNumber: String
        get() = BuildConfig.VERSION_CODE.toString()

    override val appVersion: String
        get() = BuildConfig.VERSION_NAME

    override fun getApplicationId(): String {
        return BuildConfig.APPLICATION_ID
    }

    private lateinit var userInformation: UserInformation

    override fun setUserInfo(userInformation: UserInformation) {
        this.userInformation = userInformation
    }

    override fun getUserInfo(): UserInformation? {
        return if (::userInformation.isInitialized) {
            userInformation
        } else {
            null
        }
    }

}