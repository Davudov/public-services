package com.serviceportal.psm

import android.os.Bundle
import com.serviceportal.core.mvvm.activity.CoreActivity
import com.serviceportal.psm.databinding.ActivityMainBinding
import com.serviceportal.psm.state.MainState
import com.serviceportal.psm.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : CoreActivity<MainState, MainViewModel>(MainViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.state = viewModel.currentState()
    }

    override fun showBusyIndicator(show: Boolean) {

    }

    override fun invalidateState(state: MainState) {

    }
}