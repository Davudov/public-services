package com.serviceportal.psm.manage

import kotlinx.coroutines.CoroutineScope

interface BaseCoroutineScope : CoroutineScope {
   override val coroutineContext: kotlin.coroutines.CoroutineContext
}