package com.serviceportal.psm.manage

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

interface IOCoroutineScope : BaseCoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + SupervisorJob() +
                CoroutineExceptionHandler { _, throwable ->
                    Timber.e(throwable)
                }

}