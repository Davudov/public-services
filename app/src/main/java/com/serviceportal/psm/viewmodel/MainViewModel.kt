package com.serviceportal.psm.viewmodel

import androidx.lifecycle.SavedStateHandle
import com.serviceportal.core.mvvm.viewmodel.CoreViewModel
import com.serviceportal.psm.state.MainState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject constructor(
    savedStateHandle: SavedStateHandle,
) : CoreViewModel<MainState>(MainState::class, savedStateHandle) {

}