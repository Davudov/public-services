package com.serviceportal.psm.app

import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner
import com.serviceportal.common.actions.AnalyticsHandler
import com.serviceportal.core.app.CoreApp
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class MainApp : CoreApp(), LifecycleObserver {

    @Inject
    lateinit var analyticsHandler: AnalyticsHandler

    override fun onCreate() {
        super.onCreate()
        Timber.d("Initialization app onCreate")
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        // Enforce disabling of night mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        initAnalyticsHelper()

    }

    private fun initAnalyticsHelper() {
        analyticsHandler.initAnalytics(this)
    }

}