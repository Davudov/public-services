package com.serviceportal.annotationprocessor


import com.google.auto.service.AutoService
import com.serviceportal.annotations.Arg
import com.serviceportal.annotations.ViewState
import com.squareup.javapoet.*
import com.squareup.kotlinpoet.asTypeName
import com.squareup.kotlinpoet.metadata.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import java.util.*
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.inject.Inject
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.Modifier
import javax.lang.model.element.TypeElement
import javax.tools.Diagnostic

/**
 * Annotation processor for view state bound view model creation
 * DEBUG with ./gradlew --no-daemon -Dorg.gradle.debug=true :app:clean :app:compileDebugJavaWithJavac
 */
@KotlinPoetMetadataPreview
@AutoService(Processor::class)
class AnnotationProcessorJava : AbstractProcessor() {

    companion object {
        const val STATE_FACTORY_NAME = "com.serviceportal.core.di.state.StateFactory"
        const val STATE_NAME = "com.serviceportal.core.di.state.State"
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(ViewState::class.java.name)
    }

    override fun getSupportedSourceVersion(): SourceVersion = SourceVersion.latest()

    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        roundEnv.getElementsAnnotatedWith(ViewState::class.java)
            .forEach {
                if (it !is TypeElement) {
                    processingEnv.messager.printMessage(
                        Diagnostic.Kind.ERROR,
                        "Only kotlin classes can be annotated"
                    )
                    return true
                }
                processAnnotation(it)
            }
        return true
    }

    private fun processAnnotation(element: TypeElement) {

        // Get constructor parameters
        val kmClass = element.toImmutableKmClass()
        val constructorParams = arrayListOf<ImmutableKmValueParameter>()
        kmClass.constructors.forEach { immutableKmConstructor: ImmutableKmConstructor ->
            immutableKmConstructor.valueParameters.forEach { constructorParams.add(it) }
        }

        // Fields
        val classNameText = element.simpleName.toString()
        val pack = processingEnv.elementUtils.getPackageOf(element).toString()
        val className = ClassName.get("", "$pack.$classNameText")

        // State factory
        val fileName = "${classNameText}Factory"


        // Bind module for state factory
        val bindModuleFileName = "${classNameText}BindModule"


        // Check if data class
        if (kmClass.isData.not()) {
            processingEnv.messager.printMessage(
                Diagnostic.Kind.ERROR,
                "${element.simpleName} must be a data class",
                element
            )
        }

        // Create state factory
        val stateClassBuilder =
            createStateFactory(element, constructorParams, fileName, className, classNameText)

        // Create bind module
        val stateBindClassBuilder = createStateFactoryBindModule(
            pack,
            fileName,
            bindModuleFileName,
            classNameText
        )

        // Write to file
        val fileBuilder = JavaFile.builder(pack, stateClassBuilder.build()).build()
        val bindModuleFileBuilder = JavaFile.builder(pack, stateBindClassBuilder.build()).build()

        fileBuilder.writeTo(processingEnv.filer)
        bindModuleFileBuilder.writeTo(processingEnv.filer)
    }

    private fun createStateFactoryBindModule(
        pack: String,
        fileNameOfFactory: String,
        fileName: String,
        classNameText: String
    ): TypeSpec.Builder {
        return TypeSpec.classBuilder(fileName)
            .addModifiers(Modifier.ABSTRACT, Modifier.PUBLIC)
            .addAnnotation(
                AnnotationSpec.builder(ClassName.get("", Module::class.java.canonicalName)).build()
            )
            .addAnnotation(
                AnnotationSpec.builder(ClassName.get("", InstallIn::class.java.canonicalName))
                    .addMember("value", "${SingletonComponent::class.java.canonicalName}.class")
                    .build()
            )
            .addMethod(
                MethodSpec.methodBuilder("bind${classNameText}Factory")
                    .addModifiers(Modifier.ABSTRACT, Modifier.PUBLIC)
                    .addAnnotation(
                        AnnotationSpec.builder(ClassName.get("", IntoSet::class.java.canonicalName))
                            .build()
                    )
                    .addAnnotation(
                        AnnotationSpec.builder(ClassName.get("", Binds::class.java.canonicalName))
                            .build()
                    )
                    .addParameter(ClassName.get("", "$pack.$fileNameOfFactory"), "factory")
                    .returns(
                        ParameterizedTypeName.get(
                            ClassName.get("", STATE_FACTORY_NAME),
                            ClassName.get("", STATE_NAME)
                        )
                    )
                    .build()
            )
    }

    private fun createStateFactory(
        element: Element,
        constructorParams: List<ImmutableKmValueParameter>,
        fileName: String,
        className: ClassName,
        classNameText: String
    ): TypeSpec.Builder {
        // Create class
        val classBuilder = TypeSpec.classBuilder(fileName)
            .addModifiers(Modifier.PUBLIC)
            .addMethod(
                MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .addAnnotation(
                        AnnotationSpec.builder(
                            ClassName.get(
                                "",
                                Inject::class.java.canonicalName
                            )
                        ).build()
                    )
                    .build()
            )
            .addSuperinterface(
                ParameterizedTypeName.get(
                    ClassName.get("", STATE_FACTORY_NAME),
                    ClassName.get("", STATE_NAME)
                )
            )
            .addMethod(
                MethodSpec.methodBuilder("getStateClass")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(
                        ParameterizedTypeName.get(
                            ClassName.get("java.lang", "Class"),
                            WildcardTypeName.subtypeOf(className)
                        )
                    )
                    .addAnnotation(Override::class.java)
                    .addCode("return ${classNameText}.class;")
                    .build()
            )

        // Create state
        val codeBlock =
            CodeBlock.builder().addStatement("\$T state = new \$T()", className, className)
        val fieldNames = constructorParams.map { it.name }
        // Fields
        val elements = element.enclosedElements
            .filter { enclosedElement ->
                enclosedElement.kind == ElementKind.FIELD && fieldNames.contains(enclosedElement.simpleName.toString())
            }
        // Return state
        codeBlock.add("return state.copy(")
        elements.forEachIndexed { index, enclosedElement ->
            val field = enclosedElement.simpleName.toString()
            val inIsForm = field.startsWith("is") && field.getOrNull(2)?.isUpperCase() == true
            val getter =
                if (inIsForm) "state.$field()" else "state.get${field.capitalize(Locale.ENGLISH)}()"
            val argAnnotation = enclosedElement.getAnnotation(Arg::class.java)
            if (argAnnotation != null) {
                val key =
                    if (argAnnotation.argName.isEmpty()) enclosedElement.simpleName else argAnnotation.argName
                codeBlock.add(
                    """
                        (savedStateHandle!=null && savedStateHandle.contains("$key")) ? savedStateHandle.get("$key") : $getter
                    """.trimIndent() + if ((index + 1) < elements.size) "," else ""
                )
            } else {
                codeBlock.add(getter + if ((index + 1) < elements.size) "," else "")
            }
        }
        codeBlock.add(");")

        // Add method
        classBuilder.addMethod(
            MethodSpec.methodBuilder("getState")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(
                    ClassName.get("", "androidx.lifecycle" + "." + "SavedStateHandle"),
                    "savedStateHandle"
                )
                .returns(ClassName.get("", STATE_NAME))
                .addAnnotation(Override::class.java)
                .addCode(codeBlock.build())
                .build()
        )

        // Observers add method
        // Add remove observers method
        val codeBlockAdd = CodeBlock.builder()
        codeBlockAdd.addStatement("\$T state = (\$T) s", className, className)
        elements.filter {
            it.asType().asTypeName().toString().contains("androidx.databinding.Observable")
        }.forEachIndexed { index, enclosedElement ->
            val field = enclosedElement.simpleName.toString()
            val inIsForm = field.startsWith("is") && field.getOrNull(2)?.isUpperCase() == true
            val getter =
                if (inIsForm) "state.$field()" else "state.get${field.capitalize(Locale.ENGLISH)}()"
            codeBlockAdd.addStatement("$getter.addOnPropertyChangedCallback(callback)")
        }
        classBuilder.addMethod(
            MethodSpec.methodBuilder("addObservers")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(ClassName.get("", STATE_NAME), "s")
                .addParameter(
                    ClassName.get(
                        "",
                        "androidx.databinding" + "." + "Observable.OnPropertyChangedCallback"
                    ), "callback"
                )
                .addAnnotation(Override::class.java)
                .addCode(codeBlockAdd.build())
                .build()
        )

        // Add remove observers method
        val codeBlockRemove = CodeBlock.builder()
        codeBlockRemove.addStatement("\$T state = (\$T) s", className, className)
        elements.filter {
            it.asType().asTypeName().toString().contains("androidx.databinding.Observable")
        }.forEachIndexed { index, enclosedElement ->
            val field = enclosedElement.simpleName.toString()
            val inIsForm = field.startsWith("is") && field.getOrNull(2)?.isUpperCase() == true
            val getter =
                if (inIsForm) "state.$field()" else "state.get${field.capitalize(Locale.ENGLISH)}()"
            codeBlockRemove.addStatement("$getter.removeOnPropertyChangedCallback(callback)")
        }
        classBuilder.addMethod(
            MethodSpec.methodBuilder("removeObservers")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(ClassName.get("", STATE_NAME), "s")
                .addParameter(
                    ClassName.get(
                        "",
                        "androidx.databinding" + "." + "Observable.OnPropertyChangedCallback"
                    ), "callback"
                )
                .addAnnotation(Override::class.java)
                .addCode(codeBlockRemove.build())
                .build()
        )

        return classBuilder
    }

}