package com.serviceportal.psm.build

import org.gradle.api.artifacts.dsl.DependencyHandler

object Dependencies {

    object Kotlin {
        private const val version = "1.5.10"
        const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
    }

    object Appcompat {
        private const val version = "1.2.0"
        const val appcompat = "androidx.appcompat:appcompat:$version"
    }

    object Paging3 {
        private const val version = "3.0.0"
        const val paging3 = "androidx.paging:paging-runtime-ktx:$version"
    }

    object CoreKTX {
        private const val version = "1.3.2"
        const val coreKTX = "androidx.core:core-ktx:$version"
    }

    object Firebase{
        const val platform = "com.google.firebase:firebase-bom:29.0.3"
        const val analytics = "com.google.firebase:firebase-analytics-ktx"
        const val crashlytics = "com.google.firebase:firebase-crashlytics-ktx"
        const val messaging = "com.google.firebase:firebase-messaging-ktx"
        const val perf = "com.google.firebase:firebase-perf-ktx"
        const val dynamic = "com.google.firebase:firebase-dynamic-links-ktx"
        const val iid = "com.google.firebase:firebase-iid:21.1.0"
        const val await = "org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.1.1"
    }
    object PlayServices{
        const val authApiPhone = "com.google.android.gms:play-services-auth-api-phone:18.0.1"
    }

    object Dagger {
        const val version = "2.41"
        const val hilt = "com.google.dagger:hilt-android:$version"
        const val testing = "com.google.dagger:hilt-android-testing:$version"
        const val processor = "com.google.dagger:hilt-android-compiler:$version"
        const val jetpackProcessor = "androidx.hilt:hilt-compiler:1.0.0-beta01"
        const val fragment = "androidx.hilt:hilt-navigation-fragment:1.0.0-beta01"
    }

    object Epoxy {
        private const val version = "4.5.0"
        const val epoxy = "com.airbnb.android:epoxy:$version"
        const val paging = "com.airbnb.android:epoxy-paging:$version"
        const val paging3 = "com.airbnb.android:epoxy-paging3:$version"
        const val dataBinding = "com.airbnb.android:epoxy-databinding:$version"
        const val processor = "com.airbnb.android:epoxy-processor:$version"
    }

    object Network {
        //Retrofit
        const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:4.9.0"
        const val okHttp = "com.squareup.okhttp3:okhttp:4.9.0"
        const val retrofit = "com.squareup.retrofit2:retrofit:2.9.0"
        const val retrofitConverter = "com.squareup.retrofit2:converter-gson:2.9.0"


//        testImplementation 'com.squareup.okhttp3:mockwebserver:3.2.0'
//        implementation 'com.squareup.retrofit2:converter-gson:2.6.0'
//        implementation 'com.squareup.retrofit2:adapter-rxjava2:2.6.0'
//        implementation 'commons-codec:commons-codec:1.10'
    }

    object Navigation {
        private const val version = "2.3.5"
        const val fragment = "androidx.navigation:navigation-fragment-ktx:$version"
        const val ktx = "androidx.navigation:navigation-ui-ktx:$version"
    }

    object View {
        private const val materialVersion = "1.3.0-alpha03"
        private const val constraintVersion = "2.1.2"

        const val materialDesign = "com.google.android.material:material:$materialVersion"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:$constraintVersion"
        const val kenBurns = "com.flaviofaria:kenburnsview:1.0.7"
        const val maps = "com.google.android.gms:play-services-maps:17.0.0"
    }

    object Map {
        const val core = "com.google.android.gms:play-services-maps:17.0.0"
        const val location = "com.google.android.gms:play-services-location:17.1.0"
        const val utils = "com.google.maps.android:android-maps-utils:0.5"
    }

    object Lokalize {
        private const val lokalizeVersion = "2.0.0-beta-10"
        const val lokalizeLib = "com.lokalise.android:sdk:$lokalizeVersion"
    }

    val appLibraries = arrayListOf<String>().apply {
        add(Kotlin.kotlin)
        add(Appcompat.appcompat)
        add(CoreKTX.coreKTX)
        add(Navigation.fragment)
        add(Navigation.ktx)
        add(View.materialDesign)
        add(View.constraintLayout)
        add(View.kenBurns)
        add(Map.core)
        add(Map.location)
        add(Map.utils)
        add(Dagger.hilt)
        add(Dagger.fragment)
        add(Network.retrofit)
        add(Network.loggingInterceptor)
        add(Network.okHttp)
        add(Network.retrofitConverter)
        add(Epoxy.epoxy)
        add(Epoxy.dataBinding)
        add(Epoxy.paging)
        add(Epoxy.paging3)
        add(Paging3.paging3)
    }

//    val appKaptLibraries = arrayListOf<String>().apply {
//        add(Dagger.processor)
//        add(Dagger.jetpackProcessor)
//        add(Modules.coreModuleProcessor)
//        add(Epoxy.processor)
//    }
}

fun DependencyHandler.kapt(list: List<String>) {
    list.forEach { dependency ->
        add("kapt", dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add("implementation", dependency)
    }
}

