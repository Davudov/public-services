package com.serviceportal.psm.build

object Libs {

    object App{
        const val compileSdkVersion = 31
        const val minSdkVersion = 21
        const val targetSdkVersion = 31
    }

    object Kotlin{
        const val stdLib = "org.jetbrains.kotlin:kotlin-stdlib:1.6.21"
        const val reflect = "org.jetbrains.kotlin:kotlin-reflect:1.6.21"
    }

    object LifeCycle{
        private const val version = "2.4.0-alpha01"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
        const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
        const val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:$version"
        const val ext = "androidx.lifecycle:lifecycle-extensions:2.2.0"
    }

    object PluginURLs{
        const val jitpack = "https://jitpack.io"
        const val sonatypeRepo = "https://oss.sonatype.org/content/repositories/snapshots"
        const val sonatype = "org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.8"
        const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.21"
        const val android = "com.android.tools.build:gradle:7.2.2"
        const val butterknife = "com.jakewharton:butterknife-gradle-plugin:10.2.0"
        const val hilt = "com.google.dagger:hilt-android-gradle-plugin:${Dagger.version}"
    }

    object View{
        const val material = "com.google.android.material:material:1.4.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.0-beta4"
        const val coil = "io.coil-kt:coil:0.9.5"
    }

    object Epoxy {
        private const val version = "4.5.0"
        const val epoxy = "com.airbnb.android:epoxy:$version"
        const val paging = "com.airbnb.android:epoxy-paging:$version"
        const val dataBinding = "com.airbnb.android:epoxy-databinding:$version"
        const val processor = "com.airbnb.android:epoxy-processor:$version"
    }

    object Logging{
        const val timber = "com.jakewharton.timber:timber:4.7.1"
    }

    object Fragment {
        private const val version = "1.3.5"
        const val core = "androidx.fragment:fragment:$version"
        const val fragmentKtx = "androidx.fragment:fragment-ktx:$version"

    }

    object Dagger {
        const val version = "2.41"
        const val hilt = "com.google.dagger:hilt-android:$version"
        const val hiltCore = "com.google.dagger:hilt-core:$version"
        const val testing = "com.google.dagger:hilt-android-testing:$version"
        const val processor = "com.google.dagger:hilt-android-compiler:$version"
        const val jetpackProcessor = "androidx.hilt:hilt-compiler:1.0.0-beta01"
        const val fragment = "androidx.hilt:hilt-navigation-fragment:1.0.0-beta01"
    }


    object Nav {
        const val nav_version = "2.3.5"
        const val fragment = "androidx.navigation:navigation-fragment-ktx:$nav_version"
        const val ktx = "androidx.navigation:navigation-ui-ktx:$nav_version"
    }

    object Retrofit {
        private const val version = "2.9.0"
        const val retrofit = "com.squareup.retrofit2:retrofit:$version"
        const val retrofit_rxjava_adapter = "com.squareup.retrofit2:adapter-rxjava2:$version"
        const val gsonConverter = "com.squareup.retrofit2:converter-gson:$version"
    }

    object OkHttp {
        private const val version = "4.5.0"
        const val okhttp = "com.squareup.okhttp3:okhttp:$version"
        const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:$version"
    }

    object Coroutines {
        private const val version = "1.5.1"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
    }

    object Room{
        private const val version = "2.2.5"
        const val core = "androidx.room:room-runtime:$version"
        const val processor = "androidx.room:room-compiler:$version"
        const val coroutines = "androidx.room:room-ktx:$version"
    }


}