package com.serviceportal.psm.build

import org.gradle.api.JavaVersion

object AppConfig {

    const val compileSdk = 33

    object DefaultConfig {
        const val minSdk = 21
        const val targetSdk = 33
        const val versionCode = 1
        const val versionName = "1.0.0"
        const val applicationId = "com.serviceportal"
        const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"
    }

    object BuildTypes {
        const val release = "release"
        const val debug = "debug"
        const val releaseMinifyEnabled = true
        const val debugMinifyEnabled = false
        const val defaultProguardFile = "proguard-android-optimize.txt"
        const val proguardRules = "proguard-rules.pro"
    }

    object ProductFlavors{
        const val flavorDimensions = "default"

        const val prod = "prod"

        const val regression = "regression"
        const val regressionVersionNameSuffix = "-Test"
        const val regressionApplicationIdSuffix = ".regression"

        const val mock = "mock"
        const val mockVersionNameSuffix = "-Mock"
        const val mockApplicationIdSuffix = ".mock"

        const val dev = "dev"
        const val devVersionNameSuffix = "-Dev"
        const val devApplicationIdSuffix = ".dev"
    }

    object CompileOptions {
        val sourceCompatibility = JavaVersion.VERSION_11
        val targetCompatibility = JavaVersion.VERSION_11
    }

    object KotlinOptions {
        const val jvmTarget = "11"
    }
}