package com.serviceportal.data.di

import com.serviceportal.data.base.network.interceptors.DeviceManager
import com.serviceportal.data.base.network.interceptors.DeviceManagerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class BindModule {

    @Singleton
    @Binds
    abstract fun bindDeviceManager(tokenManager: DeviceManagerImpl): DeviceManager
}