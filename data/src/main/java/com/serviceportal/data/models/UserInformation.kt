package com.serviceportal.data.models
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class UserInformation(

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("surname")
    var surname: String? = null,

    @SerializedName("pin")
    var pin: String? = null,

    @SerializedName("phone")
    var phone: String? = null,

    @SerializedName("cifs")
    var cifs: ArrayList<String>? = null,

    @SerializedName("date_of_birth")
    var dateOfBirth: String? = null,

    @SerializedName("resident")
    var resident: Boolean? = null,

    @SerializedName("id_issuing_date")
    var idIssuingDate: String? = null,

    @SerializedName("maturity_date")
    var maturityDate: String? = null,

    @SerializedName("id_number")
    var idNumber: String? = null,

    @SerializedName("id_type")
    var idType: String? = null,

    @SerializedName("age")
    var age: Int? = null,

    @SerializedName("gender")
    var gender: String? = null,

    @SerializedName("marital_status")
    var maritalStatus: String? = null,

    @SerializedName("profile")
    var profile: UserProfile? = null,

    @SerializedName("unread_count")
    var unreadCount: Int? = null,

    @SerializedName("classification")
    var classification: String? = null

) {
    fun fixNameSurnameTitleCase() {
        val locale = Locale.ROOT
        name = name?.lowercase(locale)?.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(locale) else it.toString()
        }
        surname = surname?.lowercase(locale)?.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(locale) else it.toString()
        }
    }
}

data class UserProfile(
    @SerializedName("type")
    var type: String? = null,

    @SerializedName("features")
    var features: List<Feature>? = null
)

data class Feature(
    @SerializedName("name")
    var name: String? = null,

    @SerializedName("enabled")
    var enabled: Boolean? = null
)
