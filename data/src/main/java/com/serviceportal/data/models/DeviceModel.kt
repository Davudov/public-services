package com.serviceportal.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeviceModel(
    @SerializedName("device_model")
    var deviceModel: String? = null,
    @SerializedName("device_os")
    var deviceOs: String? = "Android",
    @SerializedName("device_os_version")
    var deviceOsVersion: String? = null,
    // device unique ide
    @SerializedName("device_code")
    var deviceCode: String? = null,
    @SerializedName("device_no")
    var deviceNo: String? = "",
    @SerializedName("device_fingerprint")
    var deviceFingerprint: String? = "",
    @SerializedName("app_version")
    var appVersion: String? = null,
    @SerializedName("fcm_token")
    var fcmToken: String? = "",
    @SerializedName("jaily_broken")
    var jailyBroken: Boolean? = false,
    @SerializedName("push_enabled")
    var pushEnabled: Boolean? = true,
    var wifiMacAddress: String? = "",
    var touchScreenId: String? = "",
) : Parcelable