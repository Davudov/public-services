package com.serviceportal.data.base.network.interceptors

import com.serviceportal.data.models.DeviceModel
import okhttp3.Interceptor

interface DeviceManager : Interceptor {

    val androidDeviceId: String

    fun setFcmToken(token: String?)

}