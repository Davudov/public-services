package com.serviceportal.data.base.network.interceptors

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import android.view.InputDevice
import com.serviceportal.data.base.config.AppConfig
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.net.NetworkInterface
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@SuppressLint("HardwareIds")
class DeviceManagerImpl @Inject constructor(
    @ApplicationContext val context: Context,
    val appConfig: AppConfig,
) : DeviceManager {

    private var fcmToken: String? = null

    companion object {
        const val ACCEPT_LANGUAGE = "Accept-Language"
        const val X_APP_VERSION = "X-App-Version"
        const val AUTHORIZATION = "Authorization"
        const val STATUS = "status"
        const val OS_TYPE_HEADER = "OsType"
        const val OS_VERSION_HEADER = "OsVersion"
        const val APP_NAME_HEADER = "AppName"
        const val APP_ID_HEADER = "AppId"
        const val APP_VERSION_HEADER = "AppVersion"
        const val APP_BUILD_NUMBER_HEADER = "AppBuildNumber"
        const val APP_FUNCTION_CODE_HEADER = "FunctionCode"
        const val APP_SCREEN_KEY_HEADER = "ScreenKey"
        const val APP_DEVICE_ID_HEADER = "DeviceFingerPrint"
        const val OS_TYPE = "Android"
        const val APP_NAME = "Public Services Mobile"
        const val APP_ID = "1"
        var FUNCTION_CODE = ""
        var SCREEN_KEY = ""
    }

    override fun setFcmToken(token: String?) {
        this.fcmToken = token
    }

    override val androidDeviceId: String by lazy {
        Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID) ?: ""
    }


    fun getTouchscreenId(): String? {

        try {
            val deviceIds = InputDevice.getDeviceIds();
            for (deviceId in deviceIds) {
                val inputDevice = InputDevice.getDevice(deviceId)
                if (inputDevice.sources and InputDevice.SOURCE_TOUCHSCREEN == InputDevice.SOURCE_TOUCHSCREEN) {
                    return inputDevice.descriptor
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
        return null
    }

    fun getMacAddress(): String? {
        val networkInterfaces: List<NetworkInterface>
        try {
            networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (ni in networkInterfaces) {
                if (ni.name.equals("wlan0", ignoreCase = true)) {
                    val b = ni.hardwareAddress
                    val builder = StringBuilder()
                    if (b != null) {
                        for (add in b) {
                            builder.append(String.format("%02X:", add))
                        }
                        return builder.deleteCharAt(builder.length - 1).toString()
                    }
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
        return "02:00:00:00:00"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
        request.header(OS_TYPE_HEADER, OS_TYPE)
        request.header(OS_VERSION_HEADER, appConfig.osVersion)
        request.header(APP_NAME_HEADER, APP_NAME)
        request.header(APP_ID_HEADER, appConfig.getApplicationId())
        request.header(APP_VERSION_HEADER, appConfig.appVersion)
        request.header(APP_BUILD_NUMBER_HEADER, appConfig.appBuildNumber)
        request.header(APP_FUNCTION_CODE_HEADER, FUNCTION_CODE)
        request.header(APP_SCREEN_KEY_HEADER, SCREEN_KEY)
        request.header(APP_DEVICE_ID_HEADER, androidDeviceId)
        request.header(ACCEPT_LANGUAGE, Locale.getDefault().language)
        request.header(X_APP_VERSION, appConfig.appVersion)
        return chain.proceed(request.build())
    }

}