package com.serviceportal.data.base.config

import com.serviceportal.data.models.UserInformation

interface AppConfig {
    val isProduction: Boolean
    val osVersion: String
    val appBuildNumber: String
    val appVersion: String
    fun getApplicationId(): String
    fun setUserInfo(userInformation: UserInformation)
    fun getUserInfo(): UserInformation?
}